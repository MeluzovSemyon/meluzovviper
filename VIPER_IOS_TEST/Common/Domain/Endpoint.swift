fileprivate let base = "https://translate.yandex.net/api/v1.5/tr.json"

struct Endpoint {
    
    static let translate = base + "/translate"
    static let languages = base + "/getLangs"
}
