import Foundation

protocol IQueryCancellable {
    func cancel()
}

class Query<Response: Decodable>: IQueryCancellable {
    
    // MARK: - Typealias
    
    typealias ResultCompletion = (_ result: Result<Response>) -> ()
    
    // MARK: - Properties
        
    private var task: URLSessionTask?
    private var resource: String
    private var parameters: [String : String]
        
    // MARK: - Initialization
    
    init(to resource: String, parameters: [String : String]) {
        self.resource = resource
        self.parameters = parameters
    }
    
    convenience init(to resource: String) {
        self.init(to: resource, parameters: [:])
    }

    // MARK: - Perform
    
    func perform(completion: @escaping ResultCompletion) {
        
        performRequest { data, response, error in
            func behaviorOnError(error: Error? = nil, statusCode: Int? = nil) {
                if let statusCode = statusCode{
                    switch statusCode {
                    case 401:
                        completion(.failure(.wrongApiKey))
                        return
                    case 402:
                        completion(.failure(.apiKeyBlocked))
                        return
                    case 404:
                        completion(.failure(.translationLimit))
                        return
                    case 413:
                        completion(.failure(.textWrongSize))
                        return
                    case 422:
                        completion(.failure(.canNotBeTranslated))
                        return
                    case 501:
                        completion(.failure(.wrongDirection))
                        return
                    default:
                        completion(.failure(.noInternetConnection))
                        return
                    }
                }
                
                if let error = error {
                    if (error as NSError).code == NSURLErrorCancelled {
                        return
                    } else {
                        completion(.failure(.other(error.localizedDescription)))
                    }
                }
            }
            
            DispatchQueue.main.async {
                if let error = error {
                    behaviorOnError(error: error)
                    return
                }
                
                guard let response = response as? HTTPURLResponse else {
                    completion(.failure(.unownedErrors))
                    return
                }
                
                guard let data = data else {
                    behaviorOnError(statusCode: response.statusCode)
                    return
                }
                
                let decoder = JSONDecoder()
                guard let result = try? decoder.decode(Response.self, from: data) else {
                    return
                }
                
                completion(.success(result))
            }
        }
    }
    
    internal func performRequest(completion: @escaping (Data?, URLResponse?, Error?) -> ()) {
        guard let url = URL(string: self.resource) else {
            return
        }
        var request = URLRequest(url: url)
        request.httpMethod = "POST"
        request.setValue("application/x-www-form-urlencoded", forHTTPHeaderField: "Content-Type")
        
        var bodyString = ""

        parameters.forEach { (key, value) in
            bodyString += "\(key)=\(value)&"
        }
        bodyString.removeLast()
        
        if let parametersData = bodyString.data(using: .ascii) {
            request.httpBody = parametersData
        }
        
        task = URLSession.shared.dataTask(with: request) { data, response, error in
            completion(data, response, error)
        }
        
        task?.resume()
    }
    
    // MARK: - IQueryTask implementation
    
    func cancel() {
        task?.cancel()
    }
}
