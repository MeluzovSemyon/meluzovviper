enum Result<T> {
    case success(T)
    case failure(ResultError?)
}

enum ResultError: Error {
    case unownedErrors
    case wrongApiKey
    case apiKeyBlocked
    case translationLimit
    case textWrongSize
    case canNotBeTranslated
    case wrongDirection
    case noInternetConnection
    case other(String)
    
    var description: String {
        switch self {
        case .unownedErrors: return "Произошла неизвестная ошибка"
        case .wrongApiKey: return "Неправильный API-ключ"
        case .apiKeyBlocked: return "API-ключ заблокирован"
        case .translationLimit: return "Превышено суточное ограничение на объем переведенного текста"
        case .textWrongSize: return "Превышен максимально допустимый размер текста"
        case .canNotBeTranslated: return "Текст не может быть переведен"
        case .wrongDirection: return "Заданное направление перевода не поддерживается"
        case .noInternetConnection: return "Проверьте подключение к интернету и повторите попытку"
        case .other(let comment): return comment
        }
    }
}
