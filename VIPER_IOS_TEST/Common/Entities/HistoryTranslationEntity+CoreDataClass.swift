//
//  HistoryTranslationEntity+CoreDataClass.swift
//  VIPER_IOS_TEST
//
//  Created by Semen on 02/09/2019.
//  Copyright © 2019 MeluzovSemyon. All rights reserved.
//
//

import Foundation
import CoreData

@objc(HistoryTranslationEntity)
public class HistoryTranslationEntity: NSManagedObject {

    convenience init(text: String, translation: String, firstLanguage: LanguageEntity, secondLanguage: LanguageEntity) {
        self.init(entity: CoreDataManager.shared.entity(with: "HistoryTranslationEntity"), insertInto: CoreDataManager.shared.context)
        self.text = text
        self.translation = translation
        self.firstLanguage = firstLanguage
        self.secondLanguage = secondLanguage
        self.date = Date() as NSDate
    }
}
