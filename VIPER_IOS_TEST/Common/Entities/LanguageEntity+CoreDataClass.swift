//
//  LanguageEntity+CoreDataClass.swift
//  VIPER_IOS_TEST
//
//  Created by Semen on 02/09/2019.
//  Copyright © 2019 MeluzovSemyon. All rights reserved.
//
//

import Foundation
import CoreData

@objc(LanguageEntity)
public class LanguageEntity: NSManagedObject {

    convenience init() {
        self.init(entity: CoreDataManager.shared.entity(with: "LanguageEntity"), insertInto: CoreDataManager.shared.context)
    }
}
