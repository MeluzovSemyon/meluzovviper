//
//  HistoryTranslationEntity+CoreDataProperties.swift
//  VIPER_IOS_TEST
//
//  Created by Semen on 02/09/2019.
//  Copyright © 2019 MeluzovSemyon. All rights reserved.
//
//

import Foundation
import CoreData


extension HistoryTranslationEntity {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<HistoryTranslationEntity> {
        return NSFetchRequest<HistoryTranslationEntity>(entityName: "HistoryTranslationEntity")
    }

    @NSManaged public var text: String
    @NSManaged public var translation: String
    @NSManaged public var date: NSDate?
    @NSManaged public var firstLanguage: LanguageEntity
    @NSManaged public var secondLanguage: LanguageEntity

}
