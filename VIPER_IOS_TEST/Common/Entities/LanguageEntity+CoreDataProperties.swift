//
//  LanguageEntity+CoreDataProperties.swift
//  VIPER_IOS_TEST
//
//  Created by Semen on 02/09/2019.
//  Copyright © 2019 MeluzovSemyon. All rights reserved.
//
//

import Foundation
import CoreData


extension LanguageEntity {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<LanguageEntity> {
        return NSFetchRequest<LanguageEntity>(entityName: "LanguageEntity")
    }
    
    @NSManaged public var shortName: String
    @NSManaged public var name: String

}
