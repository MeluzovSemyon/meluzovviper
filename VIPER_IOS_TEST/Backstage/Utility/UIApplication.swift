import Foundation
import UIKit

extension UIApplication {

    class func visibleViewController(controller: UIViewController? = UIApplication.shared.keyWindow?.rootViewController) -> UIViewController? {
        if let navigationController = controller as? UINavigationController {
            return visibleViewController(controller: navigationController.visibleViewController)
        }
        if let tabController = controller as? UITabBarController {
            return visibleViewController(controller: tabController.selectedViewController)
        }
        if let presented = controller?.presentedViewController {
            return visibleViewController(controller: presented)
        }
        return controller
    }
}
