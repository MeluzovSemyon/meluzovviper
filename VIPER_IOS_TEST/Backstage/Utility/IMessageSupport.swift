import Foundation
import UIKit

protocol IMessageSupport: class {
    
    // MARK: - Handle message
    
    func show(error: ResultError?)
}

extension IMessageSupport {
    
    // MARK: - IMessageSupport default implementation
    
    func show(error: ResultError?) {
        guard let error = error else {
            showError(message: "Произошла неизвестная ошибка")
            return
        }
        
        showError(message: error.description)
    }
    
    private func showError(message: String) {
        let alertController = UIAlertController(title: "Ошибка", message: message, preferredStyle: UIAlertController.Style.alert)
        let okAction = UIAlertAction(title: "OK", style: .default, handler: nil)
        
        alertController.addAction(okAction)
        
        if let viewController = UIApplication.visibleViewController(),
            !(viewController is UIAlertController) {
            viewController.present(alertController, animated: true, completion: nil)
        }
    }
}

