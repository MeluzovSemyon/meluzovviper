import UIKit

extension UITextView {
    
    func isEmptyIncludeWhitespace() -> Bool {
        if let text = self.text, text.trimmingCharacters(in: .whitespacesAndNewlines) != "" {
            return false
        }
        return true
    }
}
