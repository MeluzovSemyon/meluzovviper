import UIKit

extension UIStoryboard {
    
    static func makeViewController<T: UIViewController>(storyboardName: String) -> T {
        let storyboard = UIStoryboard(name: storyboardName, bundle: Bundle.main)
        return storyboard.instantiateViewController(withIdentifier: String(describing: T.self)) as! T
    }
}
