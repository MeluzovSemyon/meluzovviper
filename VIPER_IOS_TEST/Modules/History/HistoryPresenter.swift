import UIKit

enum HistoryTranslationUpdateType {
    case insert
    case delete
}

struct HistoryTranslation {
    let originalText: String
    let translationText: String
}

class HistoryPresenter: IHistoryPresenterProtocol {
    
    // MARK: - Properties
    
    weak var view: IHistoryViewProtocol?
    var interactor: IHistoryInputInteractorProtocol?
    var assembly: IHistoryAssemblyProtocol?
    
    // MARK: - IHistoryPresenterProtocol implementation
    
    func loadHistory() {
        interactor?.loadHistory()
    }
    
    func removeAll() {
        interactor?.removeAll()
    }
    
    func didSelectTranslation(at index: Int) {
        guard let historyTranslationEntity = interactor?.historyTranslationEntity(at: index) else {
            return
        }
        assembly?.goToTranslationTabBarItem(with: historyTranslationEntity)
    }
}

// MARK: - IHistoryOutputInteractorProtocol

extension HistoryPresenter: IHistoryOutputInteractorProtocol {
    
    func show(translations: [HistoryTranslationEntity]) {
        var dataSource = [HistoryTranslation]()
        translations.forEach { historyTranslationEntity in
            dataSource.append(HistoryTranslation(originalText: historyTranslationEntity.text, translationText: historyTranslationEntity.translation))
        }
        view?.show(translations: dataSource)
    }
    
    func update(translation: HistoryTranslationEntity, indexPath: IndexPath, type: HistoryTranslationUpdateType) {
        let historyTranslation = HistoryTranslation(originalText: translation.text, translationText: translation.translation)
        switch type {
        case .insert:
            view?.insert(translation: historyTranslation, at: indexPath)
        case .delete:
            view?.delete(translation: historyTranslation, at: indexPath)
        }
    }
    
    func beginUpdatesHistory() {
        view?.beginUpdatesHistory()
    }
    
    func endUpdatesHistory() {
        view?.endUpdatesHistory()
    }
}
