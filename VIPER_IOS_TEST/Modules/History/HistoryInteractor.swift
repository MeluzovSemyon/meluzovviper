import Foundation

class HistoryInteractor: IHistoryInputInteractorProtocol {
    
    // MARK: - Properties
    
    weak var presenter: IHistoryOutputInteractorProtocol?

    private var historyDataManager: IHistoryDataManager = HistoryDataManager()
    
    // MARK: - Initialization
    
    init() {
        configure()
    }

    // MARK: - Configuration
    
    private func configure() {
        historyDataManager.delegate = self
    }
    
    // MARK: - IHistoryInputInteractorProtocol implementaion
    
    func historyTranslationEntity(at index: Int) -> HistoryTranslationEntity? {
        return historyDataManager.getEntity(at: index)
    }
    
    func loadHistory() {
        historyDataManager.getAllHistoryTranslation { [weak self] translations in
            self?.presenter?.show(translations: translations)
        }
    }
    
    func removeAll() {
        historyDataManager.removeAllData()
    }
}

// MARK: - IHistoryServiceDelegate

extension HistoryInteractor: IHistoryDataManagerDelegate {
    
    func beginUpdatesHistory() {
        presenter?.beginUpdatesHistory()
    }
    
    func update(translation: HistoryTranslationEntity, indexPath: IndexPath, type: HistoryTranslationUpdateType) {
        presenter?.update(translation: translation, indexPath: indexPath, type: type)
    }
    
    func endUpdatesHistory() {
        presenter?.endUpdatesHistory()
    }
}
