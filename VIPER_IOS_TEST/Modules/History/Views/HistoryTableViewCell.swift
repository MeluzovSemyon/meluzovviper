import UIKit

class HistoryTableViewCell: UITableViewCell {
    
    // MARK: - Outlets
    
    @IBOutlet weak var originalTextLabel: UILabel!
    @IBOutlet weak var translationLabel: UILabel!

    // MARK: - Configuration
    
    internal func configure(text: String?, translation: String?) {
        originalTextLabel.text = text
        translationLabel.text = translation
    }
}

