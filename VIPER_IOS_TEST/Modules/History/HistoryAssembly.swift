import UIKit

class HistoryAssembly: IHistoryAssemblyProtocol {

    // MARK: - Properties
    
    weak var historyDelegate: IHistoryDelegate? = nil
    
    // MARK: - Configuration

    static func assembleModule(with viewController: HistoryViewController, historyDelegate: IHistoryDelegate) {
        let presenter = HistoryPresenter()
        let interactor = HistoryInteractor()
        let assembly = HistoryAssembly()

        viewController.presenter = presenter
        presenter.interactor = interactor
        presenter.view = viewController
        presenter.assembly = assembly
        interactor.presenter = presenter
        assembly.historyDelegate = historyDelegate
    }
    
    func goToTranslationTabBarItem(with historyTranslation: HistoryTranslationEntity) {
        historyDelegate?.show(historyTranslation: historyTranslation)
    }
}
