import UIKit

// MARK: - Presenter -> View

protocol IHistoryViewProtocol: class {
    func show(translations: [HistoryTranslation])
    func insert(translation: HistoryTranslation, at indexPath: IndexPath)
    func delete(translation: HistoryTranslation, at indexPath: IndexPath)
    func beginUpdatesHistory()
    func endUpdatesHistory()
}

// MARK: - View -> Presenter

protocol IHistoryPresenterProtocol: class {
    func loadHistory()
    func removeAll()
    func didSelectTranslation(at index: Int)
}

// MARK: - Interactor -> Presenter

protocol IHistoryOutputInteractorProtocol: class {
    func show(translations: [HistoryTranslationEntity])
    func update(translation: HistoryTranslationEntity, indexPath: IndexPath, type: HistoryTranslationUpdateType)
    func beginUpdatesHistory()
    func endUpdatesHistory()
}

// MARK: - Presenter -> Interactor

protocol IHistoryInputInteractorProtocol {
    func historyTranslationEntity(at index: Int) -> HistoryTranslationEntity?
    func loadHistory()
    func removeAll()
}

// MARK: - Assembly

protocol IHistoryAssemblyProtocol: class {
    static func assembleModule(with viewController: HistoryViewController, historyDelegate: IHistoryDelegate)
    func goToTranslationTabBarItem(with historyTranslation: HistoryTranslationEntity)
}

// MARK: - Module

protocol IHistoryDelegate: class {
    func show(historyTranslation: HistoryTranslationEntity)
}


