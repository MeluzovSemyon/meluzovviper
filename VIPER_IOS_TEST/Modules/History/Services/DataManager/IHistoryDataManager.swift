import Foundation

protocol IHistoryDataManager {
    
    typealias HistoryTranslationsCompletion = ((_ translations: [HistoryTranslationEntity]) -> ())

    var delegate: IHistoryDataManagerDelegate? { get set }

    func getAllHistoryTranslation(completion: @escaping HistoryTranslationsCompletion)
    func getEntity(at index: Int) -> HistoryTranslationEntity?
    func removeAllData()
}

protocol IHistoryDataManagerDelegate: class {
    func beginUpdatesHistory()
    func update(translation: HistoryTranslationEntity, indexPath: IndexPath, type: HistoryTranslationUpdateType)
    func endUpdatesHistory()
}
