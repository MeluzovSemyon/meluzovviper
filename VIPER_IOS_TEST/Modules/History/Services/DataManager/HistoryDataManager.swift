import Foundation
import CoreData

class HistoryDataManager: NSObject, IHistoryDataManager {
    
    // MARK: - Properties
    
    weak var delegate: IHistoryDataManagerDelegate? = nil
    
    private var fetchedResultsController: NSFetchedResultsController<NSFetchRequestResult>!

    override init() {
        super.init()
        fetchedResultsController = CoreDataManager.shared.fetchedResultsController(entityName: "HistoryTranslationEntity")
        fetchedResultsController.delegate = self
    }
    
    // MARK: - IHistoryService implementation
    
    func getAllHistoryTranslation(completion: HistoryTranslationsCompletion) {
        
        do {
            try fetchedResultsController.performFetch()
            if let translations = fetchedResultsController.sections?[0].objects as? [HistoryTranslationEntity] {
                completion(translations)
            }
        } catch {
            print(error)
        }
    }
    
    func getEntity(at index: Int) -> HistoryTranslationEntity? {
        let fetchRequest: NSFetchRequest<NSFetchRequestResult> = HistoryTranslationEntity.fetchRequest()
        guard let objects = try? CoreDataManager.shared.context.fetch(fetchRequest) as? [HistoryTranslationEntity] else {
            return nil
        }
        return objects[index]
    }
    
    func removeAllData() {
        let fetchRequest: NSFetchRequest<NSFetchRequestResult> = HistoryTranslationEntity.fetchRequest()
        do {
            let objects = try CoreDataManager.shared.context.fetch(fetchRequest)
            for object in objects {
                guard let managedObject = object as? NSManagedObject else {
                    continue
                }
                CoreDataManager.shared.context.delete(managedObject)
                CoreDataManager.shared.saveContext()
            }
        }
        catch {
            print(error)
        }
    }
}

// MARK: - NSFetchedResultsControllerDelegate

extension HistoryDataManager: NSFetchedResultsControllerDelegate {
    
    func controllerWillChangeContent(_ controller: NSFetchedResultsController<NSFetchRequestResult>) {
        delegate?.beginUpdatesHistory()
    }
    
    func controller(_ controller: NSFetchedResultsController<NSFetchRequestResult>, didChange anObject: Any, at indexPath: IndexPath?, for type: NSFetchedResultsChangeType, newIndexPath: IndexPath?) {
        
        guard let translationEntity = anObject as? HistoryTranslationEntity else {
            return
        }
        
        switch type {
        case .insert:
            if let indexPath = newIndexPath {
                delegate?.update(translation: translationEntity, indexPath: indexPath, type: .insert)
            }
        case .delete:
            if let indexPath = indexPath {
                delegate?.update(translation: translationEntity, indexPath: indexPath, type: .delete)
            }
        default: break
        }
    }
    
    func controllerDidChangeContent(_ controller: NSFetchedResultsController<NSFetchRequestResult>) {
        delegate?.endUpdatesHistory()
    }
}
