import UIKit

class HistoryViewController: UIViewController, IHistoryViewProtocol {

    // MARK: Outlets

    @IBOutlet weak var tableView: UITableView!
    
    // MARK: - Properties
    
    var presenter: IHistoryPresenterProtocol?
    
    private var dataSource = [HistoryTranslation]()
    
    // MARK: - Life cycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        configure()
        loadHistory()
    }
    
    // MARK: - Configuration
    
    private func configure() {
        tableView.dataSource = self
        tableView.delegate = self
    }
    
    private func loadHistory() {
        presenter?.loadHistory()
    }
    
    // MARK: - IHistoryViewProtocol implementation
    
    func show(translations: [HistoryTranslation]) {
        dataSource = translations
        tableView?.reloadData()
    }
    
    func insert(translation: HistoryTranslation, at indexPath: IndexPath) {
        dataSource.insert(translation, at: indexPath.row)
        tableView?.insertRows(at: [indexPath], with: .automatic)
    }
    
    func delete(translation: HistoryTranslation, at indexPath: IndexPath) {
        dataSource.remove(at: indexPath.row)
        tableView?.deleteRows(at: [indexPath], with: .automatic)
    }
    
    func beginUpdatesHistory() {
        tableView?.beginUpdates()
    }
    
    func endUpdatesHistory() {
        tableView?.endUpdates()
    }
    
    // MARK: - Actions

    @IBAction func removeAllAction() {
        presenter?.removeAll()
    }
}

// MARK: - UITableViewDataSource

extension HistoryViewController: UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return dataSource.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "HistoryTableViewCell") as? HistoryTableViewCell else {
            return UITableViewCell()
        }
        let translation = dataSource[indexPath.row]
        cell.configure(text: translation.originalText, translation: translation.translationText)
        return cell
    }
}

// MARK: - UITableViewDelegate

extension HistoryViewController: UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        presenter?.didSelectTranslation(at: indexPath.row)
    }
}
