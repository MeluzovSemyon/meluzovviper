import UIKit

// MARK: - Presenter -> View

protocol ILanguageListViewProtocol: class {
    func show(languages: [Language])
    func showAlert(with error: ResultError?)
}

// MARK: - View -> Presenter

protocol ILanguageListPresenterProtocol: class {
    func loadLanguages()
    func didSelect(language: Language)
    func close()
}

// MARK: - Interactor -> Presenter

protocol ILanguageListOutputInteractorProtocol: class {
    func didFetch(languages: [Language])
    func showAlert(with error: ResultError?)
}

// MARK: - Presenter -> Interactor

protocol ILanguageListInputInteractorProtocol {
    func fetchLanguages()
}

// MARK: - Assembly

protocol ILanguageListAssemblyProtocol: class {
    static func assembleModule(with viewController: LanguageListViewController, languageListDelegate: ILanguageListDelegate)
    func closeCurrentViewController()
}

// MARK: - Module

protocol ILanguageListDelegate: class {
    func userDidSelect(language: Language)
}
