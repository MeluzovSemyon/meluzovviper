import UIKit

class LanguageListViewController: UIViewController, ILanguageListViewProtocol, IMessageSupport {

    // MARK: Outlets

    @IBOutlet weak var tableView: UITableView!
    
    // MARK: - Properties
    
    var presenter: ILanguageListPresenterProtocol?
    private var dataSource = [Language]()
    
    // MARK: - Life cycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        configure()
        loadLanguages()
    }
    
    // MARK: - Configuration
    
    private func configure() {
        tableView.register(UITableViewCell.self, forCellReuseIdentifier: "Cell")
        tableView.dataSource = self
        tableView.delegate = self
    }
    
    private func loadLanguages() {
        presenter?.loadLanguages()
    }
    
    // MARK: - ILanguageListViewProtocol implementation
    
    func show(languages: [Language]) {
        dataSource = languages
        tableView.reloadData()
    }
    
    func showAlert(with error: ResultError?) {
        show(error: error)
    }
    
    // MARK: - Actions

    @IBAction func closeAction() {
        presenter?.close()
    }
}

// MARK: - UITableViewDataSource

extension LanguageListViewController: UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return dataSource.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "Cell") else {
            return UITableViewCell()
        }
        cell.textLabel?.text = dataSource[indexPath.row].name
        return cell
    }
}

// MARK: - UITableViewDelegate

extension LanguageListViewController: UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        presenter?.didSelect(language: dataSource[indexPath.row])
    }
}

