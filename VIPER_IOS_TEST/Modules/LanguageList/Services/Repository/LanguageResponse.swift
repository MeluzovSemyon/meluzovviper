struct LanguageResponse: Decodable {
    let languages: [Language]
    enum CodingKeys: String, CodingKey {
        case langs
    }
    
    init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        
        let langs = try container.decode([String : String].self, forKey: .langs)
        
        let sortedKeys = Array(langs).sorted(by: {$0.1 < $1.1})
        var languages = [Language]()
        sortedKeys.forEach { (key, value) in
            languages.append(Language(short: key, name: value))
        }
        self.languages = languages
    }
}

struct Language {
    let short: String
    let name: String
}
