import Foundation

protocol ILanguageListRepository {
    
    typealias LanguageListCompletion = ((_ result: Result<[Language]>) -> ())
    
    func fetch(lang: String, completion: @escaping LanguageListCompletion)
}

final class LanguageListRepository: ILanguageListRepository {
    
    // MARK: - Properties
    
    private var currentQuery: Query<LanguageResponse>?

    // MARK: - ITranslationService implementation
    
    func fetch(lang: String, completion: @escaping LanguageListCompletion) {
        currentQuery?.cancel()
        let parameters: [String: String] = [
            "key": "trnsl.1.1.20190827T150607Z.0ed2e3eae156ce0f.c1dc68e8751d4531cd46b91469714f2d9e61149f",
            "ui": lang
        ]
        
        currentQuery = Query(to: Endpoint.languages, parameters: parameters)
        currentQuery?.perform(completion: { [unowned self] result in
            self.currentQuery = nil
            switch result {
            case .success(let response):
                completion(.success(response.languages))
            case .failure(let error):
                completion(.failure(error))
            }
        })
    }
}
