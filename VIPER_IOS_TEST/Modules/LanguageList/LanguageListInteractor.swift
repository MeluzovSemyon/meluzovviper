import Foundation

class LanguageListInteractor: ILanguageListInputInteractorProtocol {
    
    // MARK: - Properties
    
    weak var presenter: ILanguageListOutputInteractorProtocol?
    private let languageListRepository: ILanguageListRepository = LanguageListRepository()

    // MARK: - ILanguageListInputInteractorProtocol implementaion
    
    func fetchLanguages() {
        languageListRepository.fetch(lang: "ru") { [weak self] result in
            switch result {
            case .success(let languages):
                self?.presenter?.didFetch(languages: languages)
            case .failure(let error):
                self?.presenter?.showAlert(with: error)
            }
        }
    }
}
