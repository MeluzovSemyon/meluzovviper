import UIKit

class LanguageListPresenter: ILanguageListPresenterProtocol {
    
    // MARK: - Properties
    
    weak var view: ILanguageListViewProtocol?
    var interactor: ILanguageListInputInteractorProtocol?
    var assembly: ILanguageListAssemblyProtocol?

    weak var delegate: ILanguageListDelegate? = nil
    
    // MARK: - ILanguageListPresenterProtocol implementation

    func loadLanguages() {
        interactor?.fetchLanguages()
    }
    
    func didSelect(language: Language) {
        delegate?.userDidSelect(language: language)
        assembly?.closeCurrentViewController()
    }
    
    func close() {
        assembly?.closeCurrentViewController()
    }
}

// MARK: - ILanguageListOutputInteractorProtocol

extension LanguageListPresenter: ILanguageListOutputInteractorProtocol {

    func didFetch(languages: [Language]) {
        view?.show(languages: languages)
    }
    
    func showAlert(with error: ResultError?) {
        view?.showAlert(with: error)
    }
}
