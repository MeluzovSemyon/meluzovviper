import UIKit

class LanguageListAssembly: ILanguageListAssemblyProtocol {
    
    // MARK: - Properties
    
    private var viewController: UIViewController!
    
    // MARK: - Routing
    
    func closeCurrentViewController() {
        viewController.dismiss(animated: true, completion: nil)
    }
    
    // MARK: - Configuration

    static func assembleModule(with viewController: LanguageListViewController, languageListDelegate: ILanguageListDelegate) {
        let presenter = LanguageListPresenter()
        let interactor = LanguageListInteractor()
        let assembly = LanguageListAssembly()

        viewController.presenter = presenter
        presenter.interactor = interactor
        presenter.delegate = languageListDelegate
        presenter.view = viewController
        presenter.assembly = assembly
        interactor.presenter = presenter
        assembly.viewController = viewController
    }
}
