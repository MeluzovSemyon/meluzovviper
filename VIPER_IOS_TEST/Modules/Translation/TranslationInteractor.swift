import Foundation

class TranslationInteractor: ITranslationInputInteractorProtocol {
    
    // MARK: - Properties
    
    weak var presenter: ITranslationOutputInteractorProtocol?
    
    private let translationRepository: ITranslationRepository = TranslationRepository()
    private let translationDataManager: ITranslationDataManager = TranslationDataManager()

    internal var firstLanguage: Language {
        didSet {
            presenter?.setFirstLanguage(language: firstLanguage)
        }
    }
    internal var secondLanguage: Language {
        didSet {
            presenter?.setSecondLanguage(language: secondLanguage)
        }
    }

    enum ChangingLanguage {
        case first
        case second
        case none
    }
    
    private var changingLanguage: ChangingLanguage = .none
    
    // MARK: - Initialization
    
    init(firstLanguage: Language, secondLanguage: Language) {
        self.firstLanguage = firstLanguage
        self.secondLanguage = secondLanguage
    }
    
    // MARK: - ITranslationInputInteractorProtocol implementaion
    
    func startChangeFirstLanguage() {
        changingLanguage = .first
    }
    
    func startChangeSecondLanguage() {
        changingLanguage = .second
    }
    
    func userDidSelect(language: Language) {
        switch changingLanguage {
        case .first:
            if language.short == secondLanguage.short {
                secondLanguage = firstLanguage
            }
            if firstLanguage.short != language.short {
                firstLanguage = language
            }
        case .second:
            if language.short == firstLanguage.short {
                firstLanguage = secondLanguage
            }
            if secondLanguage.short != language.short {
                secondLanguage = language
            }
        default: break
        }
        
        changingLanguage = .none
    }
    
    func reverseLanguages() {
        swap(&firstLanguage, &secondLanguage)
    }
    
    func translate(text: String) {
        translationRepository.translate(text: text, firstLanguage: firstLanguage.short, secondLanguage: secondLanguage.short) { [weak self] result in
            switch result {
            case .success(let translation):
                self?.presenter?.textDidTranslate(translation: translation)
            case .failure(let error):
                self?.presenter?.show(error: error)
            }
        }
    }
    
    func save(text: String, translation: String) {
        translationDataManager.save(
            text: text,
            translation: translation,
            firstLanguage: firstLanguage,
            secondLanguage: secondLanguage
        )
    }
}
