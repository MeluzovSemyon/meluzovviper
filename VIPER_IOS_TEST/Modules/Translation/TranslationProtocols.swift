import UIKit

// MARK: - Presenter -> View

protocol ITranslationViewProtocol: class {
    func setFirstLanguage(with name: String, needTranslate: Bool)
    func setSecondLanguage(with name: String, needTranslate: Bool)
    func show(text: String?)
    func showTranslate(text: String?)
    func showAlert(with error: ResultError?)
}

// MARK: - View -> Presenter

protocol ITranslationPresenterProtocol: class {
    var text: String? { get set }
    var translation: String? { get set }
    func viewDidLoad()
    func textDidChange(text: String)
    func startChangeFirstLanguage()
    func startChangeSecondLanguage()
    func reverseLanguages()
    func save(text: String, translation: String)
}

// MARK: - Interactor -> Presenter

protocol ITranslationOutputInteractorProtocol: class {
    func setFirstLanguage(language: Language)
    func setSecondLanguage(language: Language)
    func textDidTranslate(translation: Translation)
    func show(error: ResultError?)
}

// MARK: - Presenter -> Interactor

protocol ITranslationInputInteractorProtocol {
    var firstLanguage: Language { set get }
    var secondLanguage: Language { set get }
    func startChangeFirstLanguage()
    func startChangeSecondLanguage()
    func userDidSelect(language: Language)
    func reverseLanguages()
    func translate(text: String)
    func save(text: String, translation: String)
}

// MARK: - Assembly

protocol ITransplationAssemblyProtocol: class {
    static func assembleModule(with viewController: TranslationViewController, initialFirstLanguage: Language, initialSecondLanguage: Language, text: String?, translation: String?)
    
    func goToLanguageListViewController(from view: UIViewController, languageListDelegate: ILanguageListDelegate)
}
