import UIKit

class TranslationAssembly: ITransplationAssemblyProtocol {
    
    // MARK: - Routing
    
    func goToLanguageListViewController(from view: UIViewController, languageListDelegate: ILanguageListDelegate) {
        let languageListViewController: LanguageListViewController = UIStoryboard.makeViewController(storyboardName: "Main")
        LanguageListAssembly.assembleModule(with: languageListViewController, languageListDelegate: languageListDelegate)
        view.present(languageListViewController, animated: true, completion: nil)
    }
    
    // MARK: - Configuration
    
    static func assembleModule(with viewController: TranslationViewController, initialFirstLanguage: Language, initialSecondLanguage: Language, text: String? = nil, translation: String? = nil) {
        let presenter = TranslationPresenter()
        let interactor = TranslationInteractor(firstLanguage: initialFirstLanguage, secondLanguage: initialSecondLanguage)
        let assembly = TranslationAssembly()

        viewController.presenter = presenter
        
        presenter.text = text
        presenter.translation = translation
        presenter.interactor = interactor
        presenter.view = viewController
        presenter.assembly = assembly
        
        interactor.presenter = presenter
    }
}

