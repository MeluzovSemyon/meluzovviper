import UIKit

class TranslationPresenter: ITranslationPresenterProtocol {
    
    // MARK: - Properties
    
    weak var view: ITranslationViewProtocol?
    var interactor: ITranslationInputInteractorProtocol?
    var assembly: ITransplationAssemblyProtocol?
    
    var text: String?
    var translation: String?

    // MARK: - ITranslationPresenterProtocol implementation
    
    func viewDidLoad() {
        view?.setFirstLanguage(with: interactor?.firstLanguage.name ?? "", needTranslate: false)
        view?.setSecondLanguage(with: interactor?.secondLanguage.name ?? "", needTranslate: false)
        view?.showTranslate(text: translation)
        view?.show(text: text)
    }
    
    func textDidChange(text: String) {
        interactor?.translate(text: text)
    }
    
    func startChangeFirstLanguage() {
        interactor?.startChangeFirstLanguage()
        showLanguageList()
    }
    
    func startChangeSecondLanguage() {
        interactor?.startChangeSecondLanguage()
        showLanguageList()
    }
    
    func reverseLanguages() {
        interactor?.reverseLanguages()
    }
    
    func save(text: String, translation: String) {
        interactor?.save(text: text, translation: translation)
    }
    
    // MARK: - Private methods
    
    private func showLanguageList() {
        guard let view = self.view as? UIViewController else {
            return
        }
        
        assembly?.goToLanguageListViewController(from: view, languageListDelegate: self)
    }
}

// MARK: - ITranslationOutputInteractorProtocol

extension TranslationPresenter: ITranslationOutputInteractorProtocol {
    
    func textDidTranslate(translation: Translation) {
        if let text = translation.text.first {
            view?.showTranslate(text: text)
        }
    }
    
    func setFirstLanguage(language: Language) {
        view?.setFirstLanguage(with: language.name, needTranslate: true)
    }
    
    func setSecondLanguage(language: Language) {
        view?.setSecondLanguage(with: language.name, needTranslate: true)
    }
    
    func show(error: ResultError?) {
        view?.showAlert(with: error)
    }
}

// MARK: - ILanguageListDelegate

extension TranslationPresenter: ILanguageListDelegate {
    
    func userDidSelect(language: Language) {
        interactor?.userDidSelect(language: language)
    }
}
