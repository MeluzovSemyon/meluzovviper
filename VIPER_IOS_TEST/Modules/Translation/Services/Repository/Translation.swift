struct Translation: Decodable {
    let lang: String
    let text: [String]
    
    enum CodingKeys: String, CodingKey {
        case lang
        case text
    }
}
