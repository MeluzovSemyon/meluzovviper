import Foundation

protocol ITranslationRepository {
    
    typealias TranslateCompletion = ((_ result: Result<Translation>) -> ())

    func translate(text: String, firstLanguage: String, secondLanguage: String, completion: @escaping TranslateCompletion)
}

final class TranslationRepository: ITranslationRepository {
    
    // MARK: - Properties
    
    private var currentQuery: Query<Translation>?
    
    // MARK: - ITranslationService implementation
    
    func translate(text: String, firstLanguage: String, secondLanguage: String, completion: @escaping TranslateCompletion) {
        currentQuery?.cancel()
        let parameters: [String: String] = [
            "key": "trnsl.1.1.20190827T150607Z.0ed2e3eae156ce0f.c1dc68e8751d4531cd46b91469714f2d9e61149f",
            "text": text,
            "lang": "\(firstLanguage)-\(secondLanguage)"
        ]
        
        currentQuery = Query(to: Endpoint.translate, parameters: parameters)
        currentQuery?.perform(completion: { [unowned self] result in
            self.currentQuery = nil
            completion(result)
        })
    }
}
