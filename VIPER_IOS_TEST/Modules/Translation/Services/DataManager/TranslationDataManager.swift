import Foundation
import CoreData

protocol ITranslationDataManager {
    func save(text: String, translation: String, firstLanguage: Language, secondLanguage: Language)
}

final class TranslationDataManager: ITranslationDataManager {
    
    func save(text: String, translation: String, firstLanguage: Language, secondLanguage: Language) {
        let firstLanguageEntity = LanguageEntity()
        firstLanguageEntity.shortName = firstLanguage.short
        firstLanguageEntity.name = firstLanguage.name
        
        let secondLanguageEntity = LanguageEntity()
        secondLanguageEntity.shortName = secondLanguage.short
        secondLanguageEntity.name = secondLanguage.name
        
        let _ = HistoryTranslationEntity(text: text, translation: translation, firstLanguage: firstLanguageEntity, secondLanguage: secondLanguageEntity)
        
        CoreDataManager.shared.saveContext()
    }
}
