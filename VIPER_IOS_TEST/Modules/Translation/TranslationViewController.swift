import UIKit

class TranslationViewController: UIViewController, ITranslationViewProtocol, IMessageSupport {

    // MARK: Outlets
    
    @IBOutlet weak var firstLanguageButton: UIButton!
    @IBOutlet weak var secondLanguageButton: UIButton!

    @IBOutlet weak var inputTextView: UITextView!
    @IBOutlet weak var outputLabel: UILabel!

    // MARK: - Properties
    
    var presenter: ITranslationPresenterProtocol?
    
    // MARK: - Life cycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        inputTextView.delegate = self
        presenter?.viewDidLoad()
        configure()
    }
    
    // MARK: - Configure
    
    private func configure() {
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(self.endEditing))
        tapGesture.cancelsTouchesInView = false
        view.addGestureRecognizer(tapGesture)
    }
    
    @objc func endEditing() {
        view.endEditing(true)
    }
    
    // MARK: - ITranslationViewProtocol implementation
    
    func setFirstLanguage(with name: String, needTranslate: Bool) {
        firstLanguageButton.setTitle(name, for: .normal)
        if needTranslate {
            translate()
        }
    }
    
    func setSecondLanguage(with name: String, needTranslate: Bool) {
        secondLanguageButton.setTitle(name, for: .normal)
        if needTranslate {
            translate()
        }
    }
    
    func show(text: String?) {
        inputTextView.text = text
    }
    
    func showTranslate(text: String?) {
        outputLabel.text = text
    }
    
    func showAlert(with error: ResultError?) {
        show(error: error)
    }
    
    // MARK: - Private methods
    
    private func translate() {
        presenter?.textDidChange(text: inputTextView.text)
    }
    
    // MARK: - Actions
    
    @IBAction func leftLanguageAction() {
        presenter?.startChangeFirstLanguage()
    }
    
    @IBAction func rightLanguageAction() {
        presenter?.startChangeSecondLanguage()
    }
    
    @IBAction func reverseLanguagesAction() {
        inputTextView.text = outputLabel.text
        presenter?.reverseLanguages()
    }
    
    @IBAction func addToHistoryAction() {
        guard let text = inputTextView.text,
            let translation = outputLabel.text else {
                return
        }
        presenter?.save(text: text, translation: translation)
    }
}

// MARK: - UITextViewDelegate

extension TranslationViewController: UITextViewDelegate {
    
    func textViewDidChange(_ textView: UITextView) {
        if textView.isEmptyIncludeWhitespace() {
            outputLabel.text = ""
            return
        }
        translate()
    }
}
