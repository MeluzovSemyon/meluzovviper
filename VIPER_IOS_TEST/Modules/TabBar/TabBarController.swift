import UIKit

class TabBarController: UITabBarController {
    
    // MARK: - Life cycle
    
    override func awakeFromNib() {
        super.awakeFromNib()
        configure()
        configureItems()
    }
    
    // MARK: - Configuration
    
    private func configure() {
        let translationViewController: TranslationViewController = UIStoryboard.makeViewController(storyboardName: "Main")
        let historyViewController: HistoryViewController = UIStoryboard.makeViewController(storyboardName: "Main")
        
        let historyNavigationController = UINavigationController(rootViewController: historyViewController)
        historyNavigationController.navigationBar.isTranslucent = false
        historyNavigationController.navigationBar.barTintColor = Colors.yellow
        
        let initialFirstLanguage = Language(short: "en", name: "Английский")
        let initialSecondLanguage = Language(short: "de", name: "Немецкий")
        TranslationAssembly.assembleModule(with: translationViewController, initialFirstLanguage: initialFirstLanguage, initialSecondLanguage: initialSecondLanguage)
        HistoryAssembly.assembleModule(with: historyViewController, historyDelegate: self)
        viewControllers = [translationViewController, historyNavigationController]
    }
    
    private func configureItems() {
        tabBar.items?[0].title = nil
        tabBar.items?[0].image = UIImage(named: "tabBarTranslationNormalIcon")
        tabBar.items?[0].selectedImage = UIImage(named: "tabBarTranslationSelectedIcon")
        
        tabBar.items?[1].title = nil
        tabBar.items?[1].image = UIImage(named: "tabBarDictionaryNormalIcon")
        tabBar.items?[1].selectedImage = UIImage(named: "tabBarDictionarySelectedIcon")
    }
}

extension TabBarController: IHistoryDelegate {
    
    func show(historyTranslation: HistoryTranslationEntity) {
        let translationViewController: TranslationViewController = UIStoryboard.makeViewController(storyboardName: "Main")
        
        let firstLanguage = Language(short: historyTranslation.firstLanguage.shortName, name: historyTranslation.firstLanguage.name)
        let secondLanguage = Language(short: historyTranslation.secondLanguage.shortName, name: historyTranslation.secondLanguage.name)

        TranslationAssembly.assembleModule(with: translationViewController, initialFirstLanguage: firstLanguage, initialSecondLanguage: secondLanguage, text: historyTranslation.text, translation: historyTranslation.translation)
        viewControllers?.remove(at: 0)
        viewControllers?.insert(translationViewController, at: 0)
        selectedIndex = 0
    }
}
